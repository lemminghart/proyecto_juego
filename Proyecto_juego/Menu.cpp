#include "Menu.h"
#include "GameConstants.h"

Menu::Menu() {
	_textureID = SPRITE_MENU;
	_textureID_END = SPRITE_END;
	_textureID_WIN = SPRITE_WIN;

}

Menu::~Menu() {
}

void Menu::init(SDLInterface * graphic) {
	_graphic = graphic;
}

void Menu::drawMenu() {
	_graphic->drawTexture(_textureID, 0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight(),
		0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight());
}

void Menu::drawEnd() {
	_graphic->drawTexture(_textureID_END, 0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight(),
		0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight());
}

void Menu::drawWin() {
	_graphic->drawTexture(_textureID_WIN, 0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight(),
		0, 0, _graphic->getWindowWidth(), _graphic->getWindowHeight());
}
