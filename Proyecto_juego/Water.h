#pragma once
#include <stdlib.h>
#include "GameConstants.h"
#include "Player.h"
#include "Obj.h"



class Water:public Obj {
private:
	int width;
	int height;
	int drink;
	int rang;
	std::string directionX;
	std::string directionY;

public:
	Water();
	~Water();

	int getDrink();

	//Getters
	int getWidth();
	int getHeight();
	int getPositionX();
	int getPositionY();
	
	std::string getDirectionX();
	std::string getDirectionY();

	////Setters
	void setPosition(int xValue, int yValue);

	//Function
	void newPosition(Player player);
	bool doPhysics(int xPlayer, int yPlayer);
	void calculateDirection(Player player);


};

