#pragma once
#include "Obj.h"
#include "GameConstants.h"
#include <ctime>
class Dog :
	public Obj
{
private:
	time_t timer;
	time_t act;
public:
	Dog();
	~Dog();
	void Update(int xValue, int yValue);
};

