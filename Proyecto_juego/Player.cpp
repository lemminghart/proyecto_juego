#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <string>
#include "SDLInterface.h"
#include "Player.h"
#include "Map.h"
#include "Obj.h"


Player::Player() {
	textureID = SPRITE_HERO;
	x = 0;
	y = 0;
	currentSteps = 10;
	moving = false;
	numSteps = 0;
	conditionToWin = 0; //pick up 10 bottles to open teleport
}


Player::~Player() {
}

void Player::init(SDLInterface * graphic) {
	_graphic = graphic;
}


void Player::setPosition(int xValue, int yValue) {
	x = xValue;
	y = yValue;
}

int Player::getPositionX() {
	return x;
}

int Player::getPositionY() {
	return y;
}

void Player::setMoving(int valor) {
	moving = valor;
}

void Player::setCurrentStep(int value)
{
	currentSteps = value;
}

void Player::setNumStep(int value)
{
	numSteps=value;
}

void Player::setConditionToWin(int condition)
{
	conditionToWin = condition;
}

void Player::UpdateConditionToWin()
{
	conditionToWin++;
}

int Player::getMoving() {
	return moving;
}

int Player::getCurrentStep()
{
	return currentSteps;
}

int Player::getNumStep()
{
	return numSteps;
}

int Player::getConditionToWin()
{
	return conditionToWin;
}
