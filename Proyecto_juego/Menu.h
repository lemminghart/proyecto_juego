#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <string>
#include "SDLInterface.h"


class Menu
{
	int _textureID;
	int _textureID_END;
	int _textureID_WIN;
	SDLInterface * _graphic;

	
public:
	Menu();
	~Menu();
	void init(SDLInterface * graphic);
	void drawMenu();
	void drawEnd();
	void drawWin();
};

