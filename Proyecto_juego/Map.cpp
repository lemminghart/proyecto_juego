#include "Map.h"
#include "GameConstants.h"
#include "Player.h"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;


Map::Map(int windowX, int windowY, SDLInterface * graphic) {
	_graphic = graphic;
	xCamara = 0;
	yCamara = 0;
	_player.setPosition(2, 0);
	Water w;
	Dog d;
	item.push_back(w);
	enemy.push_back(d);

	start = false;

	//	_player.setPosition((_screenWidth / SPRITE_DEFAULT_WIDTH) / 2, (_screenHeight / SPRITE_DEFAULT_HEIGHT) / 2);
	int cont = 1;
	for (int i = 0; i < MAPSIZE_X; i++) {
		for (int j = 0; j < MAPSIZE_Y; j++) {
			/*if((i==2 && j<MAPSIZE_Y-2)||(i>1 && i<MAPSIZE_X-1 && j==MAPSIZE_Y-2) || (i==MAPSIZE_X-2 && j<MAPSIZE_Y-2))data_matrix[i][j] = SPRITE_GRASS;
			else { data_matrix[i][j] = SPRITE_GRASS2; }*/
			data_matrix[i][j] = SPRITE_GRASS;
			cont++;
		}
	}

	if (MAX_BLOCK_XCAMARA == 0 || MAX_BLOCK_YCAMARA == 0) {
		MAX_BLOCK_XCAMARA = windowX / SPRITE_DEFAULT_WIDTH;
		MAX_BLOCK_YCAMARA = windowY / SPRITE_DEFAULT_HEIGHT;
	}
	
}

Map::~Map() {
}

//d
void Map::drawMap() {
	//llamar a funci�n que actualiza xCamara y yCamara segun heroe
	moveCamera(_player.getPositionX(), _player.getPositionY());

	int contI = 0;
	int contJ = 0;

	for (int i = xCamara; i <= xCamara + MAX_BLOCK_XCAMARA; i++) {
		for (int j = yCamara; j <= yCamara + MAX_BLOCK_YCAMARA; j++) {
				_graphic->drawTexture(data_matrix[i][j], 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, contI * SPRITE_DEFAULT_WIDTH, contJ * SPRITE_DEFAULT_HEIGHT, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
				contJ++;
		}
		contI++;
		contJ = 0;
	}

	drawPlayer();
	drawUI();

}

void Map::drawUI() {

	int score = 0;

	_graphic->drawText("SCORE:", WHITE, 0, 0);
	_graphic->drawText(std::to_string(getStepsPlayer()), WHITE, 80, 0);

	
	/*item[0].calculateDirection(_player);
	
	_graphic->drawText("X:", WHITE, 600, 0);
	_graphic->drawText(item[0].getDirectionX(), WHITE, 630, 0);
	_graphic->drawText("Y:", WHITE, 600, 20);
	_graphic->drawText(item[0].getDirectionY(), WHITE, 630, 20);*/
	//_graphic.drawText(text_tiempo, BLACK, 0, 0); X = 90

	for (int i = 0; i < item.size(); i++)
	{

		item[i].calculateDirection(_player);

		_graphic->drawText("X:", WHITE, 600, 0);
		_graphic->drawText(item[i].getDirectionX(), WHITE, 630, 0);
		_graphic->drawText("Y:", WHITE, 600, 20);
		_graphic->drawText(item[i].getDirectionY(), WHITE, 630, 20);
	}

}

void Map::movePlayer(int direction) {
	// UP == 1
	if (direction == 1) {
		if ((_player.getPositionY() - 1) != -1) {
			//actualizamos posici�n player
			_player.setPosition(_player.getPositionX(), (_player.getPositionY() - 1));
		//	std::cout << _player.getPositionY() << std::endl;
			decreesStep();
		}
		else {
			std::cout << "Limite" << std::endl;
		}
	}
	// left == 2
	else if (direction == 2) {
		if ((_player.getPositionX() - 1) != -1) {
			//actualizamos posici�n player
			_player.setPosition((_player.getPositionX() -1), _player.getPositionY());
		//	std::cout << _player.getPositionX() << std::endl;
			decreesStep();
		}
		else {
			std::cout << "Limite" << std::endl;
		}
	}
	// DOWN == 3
	else if (direction == 3) {
		if ((_player.getPositionY() + 1) != (MAPSIZE_Y )) {
			//actualizamos posici�n player
			_player.setPosition(_player.getPositionX(), (_player.getPositionY() + 1));
		//	std::cout << _player.getPositionY() << std::endl;
			decreesStep();
		}
		else {
			std::cout << "Limite" << std::endl;
		}
	}
	// RIGHT == 4
	else if (direction == 4) {
		if ((_player.getPositionX() + 1) != (MAPSIZE_X)) {
			//actualizamos posici�n player
			_player.setPosition((_player.getPositionX() + 1), _player.getPositionY());
		//	std::cout << _player.getPositionX() << std::endl;
			decreesStep();
		}
		else {
			std::cout << "Limite" << std::endl;
		}
		
	}
}

void Map::doPhysics()
{
	if (_player.getCurrentStep() <= 0) {
		setEnd(true);
	}
	if (_player.getConditionToWin() >= CONDITION_TO_WIN) {
		setWin(true);
	}
	for (int i = 0; i < item.size(); i++)
	{
		if (item[i].doPhysics(_player.getPositionX(), _player.getPositionY()))
		{
			_player.UpdateConditionToWin();
			_player.setCurrentStep(_player.getCurrentStep() + item[i].getDrink());
			item[i].newPosition(_player);
		//	std::cout << "agua: " << item[i].getPositionX() << " - " << item[i].getPositionY() << std::endl;
		}
	}

	if (_estimulante.doPhysics(_player.getPositionX(), _player.getPositionY()))
	{
		_estimulante.setActive(20);
		_estimulante.newPosition(_player);
		//std::cout << "estimulante: " << _estimulante.getPositionX() << " - " << _estimulante.getPositionY() << std::endl;
	}

	//AI
	for (int i = 0; i < item.size(); i++)
	{
		for (int i = 0; i < enemy.size(); i++)
		{
			if (start)enemy[i].Update(item[i].x, item[i].y);
			if (item[i].doPhysics(enemy[i].x, enemy[i].y))
			{
				item[i].newPosition(_player);
				//std::cout << "agua: " << item[i].getPositionX() << " - " << item[i].getPositionY() << std::endl;
			}
		}
	}
}

void Map::drawPlayer() {
	_graphic->drawTexture(SPRITE_HERO, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, ((_player.getPositionX() - xCamara) * 64) , ((_player.getPositionY() - yCamara) * 64), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
//	_graphic->drawTexture(SPRITE_WATER, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT,  _water.getPositionX()*64,  _water.getPositionY()*64, _water.getWidth(), _water.getHeight());
	for (int i = 0; i < item.size(); i++)
	{
		_graphic->drawTexture(SPRITE_WATER, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, ((item[i].getPositionX() - xCamara) * 64), ((item[i].getPositionY() - yCamara) * 64), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
	}
	for (int i = 0; i < enemy.size(); i++)
	{
		_graphic->drawTexture(enemy[i].GetTextureID(), 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, ((enemy[i].GetX() - xCamara) * 64), ((enemy[i].GetY() - yCamara) * 64), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
	}
	_graphic->drawTexture(SPRITE_ESTIMULANTE, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, ((_estimulante.getPositionX() - xCamara) * 64), ((_estimulante.getPositionY() - yCamara) * 64), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
}


void Map::setCamaraX(int camaraX) {
	Map::xCamara = camaraX;
}

void Map::setCamaraY(int camaraY) {
	Map::yCamara = camaraY;
}

void Map::setEnd(bool dep) {
	end = dep;
	//_player.setConditionToWin(0);
	_player.setCurrentStep(10);
	_player.setPosition(2, 0);
	item[0].setPosition(2,2);

	enemy[0].x = 8;
	enemy[0].y = 2;
}

void Map::setWin(bool wp) {
	win = wp;
	_player.setConditionToWin(0);
	_player.setCurrentStep(10);
	_player.setPosition(2, 0);
	item[0].setPosition(2, 2);
	//item[2].setPosition(5, 6);
}

bool Map::getEnd() {
	return end;
}

bool Map::getWin() {
	return win;
}

int Map::getCamaraX() {
	return xCamara;
}

int Map::getCamaraY() {
	return yCamara;
}

int Map::getStepsPlayer()
{
	return _player.getCurrentStep();

}

void Map::drawScoreAtWin() {
	
	_graphic->drawText("SCORE:", WHITE, 275, 500);
	_graphic->drawText(std::to_string(_player.getCurrentStep()), WHITE, 375, 500);
}

void Map::moveCamera(int xHeroPos, int yHeroPos)
{
	xCamara = xHeroPos - (MAX_BLOCK_XCAMARA / 2);
	yCamara = yHeroPos - (MAX_BLOCK_YCAMARA / 2);

	if (xCamara < (MAX_BLOCK_XCAMARA / 2)){
		xCamara = 0;
	}

	if (yCamara < (MAX_BLOCK_YCAMARA / 2)) {
		yCamara = 0;
	}
	int xPrueba = MAPSIZE_X - MAX_BLOCK_XCAMARA;
	if (xCamara >= (xPrueba-1)) {
		xCamara = MAPSIZE_X - MAX_BLOCK_XCAMARA-1;
	}	
	int yPrueba = MAPSIZE_Y - MAX_BLOCK_YCAMARA;
	if (yCamara >= (yPrueba)) {
		yCamara = MAPSIZE_Y - MAX_BLOCK_YCAMARA-1;
	}
}
void Map::decreesStep()
{
	if (_estimulante.getActive() > 0) {
		_estimulante.updateActive();
	}
	else {
		_player.setCurrentStep(_player.getCurrentStep() - 1);
	}
}
void Map::increesStep()
{
	_player.setNumStep(_player.getNumStep()+1);
}

void Map::saveGame() {
	ofstream file("../sharedResources/DATA/save.txt");
	if (file.is_open()) {
		//player DATA
		file << _player.getPositionX() << endl;
		file << _player.getPositionY() << endl;
		file << _player.getMoving() << endl;
		file << _player.getCurrentStep() << endl;
		file << _player.getNumStep() << endl;
		file << _player.getConditionToWin() << endl;

		//water DATA
		for (int i = 0; i < item.size(); i++)
		{
			file << item[i].getPositionX() << endl;
			file << item[i].getPositionY() << endl;
		}
		
		//estimulante DATA
		file << _estimulante.getPositionX() << endl;
		file << _estimulante.getPositionY() << endl;
		file << _estimulante.getActive() << endl;

		//camara DATA
		file << xCamara << endl;
		file << yCamara << endl;

		file.close();
	}
	else {
		cout << "Unable to open file";
	}
}

void Map::loadGame() {
	int data[13];
	string line;
	ifstream file("../sharedResources/DATA/save.txt");
	if (file.is_open()) {
		for (int i = 0; i < 13; i++) {
			file >> data[i];
		}
		_player.setPosition(data[0], data[1]);
		_player.setMoving(data[2]);
		_player.setCurrentStep(data[3]);
		_player.setNumStep(data[4]);
		_player.setConditionToWin(data[5]);

		for (int i = 0; i < item.size(); i++)
		{
			item[i].setPosition(data[6], data[7]);
		}

		_estimulante.setPosition(data[8], data[9]);
		_estimulante.setActive(data[10]);

		xCamara = data[11];
		yCamara = data[12];
	}
	else{
		cout << "Unable to open file";
	}
}
