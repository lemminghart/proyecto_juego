#pragma once
#include <stdlib.h>
#include "GameConstants.h"
#include "Player.h"
#include "Obj.h"

class Estimulante:public Obj {
private:
	int width;
	int height;
	std::string directionX;
	std::string directionY;
	int active;

public:
	Estimulante();
	~Estimulante();


	//Getters
	int getWidth();
	int getHeight();
	int getPositionX();
	int getPositionY();
	int getActive();
	

	std::string getDirectionX();
	std::string getDirectionY();

	////Setters
	void setPosition(int xValue, int yValue);
	void setActive(int aktive);

	//Function
	void newPosition(Player player);
	bool doPhysics(int xPlayer, int yPlayer);
	void calculateDirection(Player player);
	void updateActive();

};

