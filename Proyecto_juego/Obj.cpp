#include "Obj.h"



Obj::Obj()
{
}


Obj::~Obj()
{
}

void Obj::SetX(int value)
{
	x = value;
}

void Obj::SetY(int value)
{
	y = value;
}

void Obj::SetTextureID(int value)
{
	textureID = value;
}

int Obj::GetX()
{
	return x;
}

int Obj::GetY()
{
	return y;
}

int Obj::GetTextureID()
{
	return textureID;
}
