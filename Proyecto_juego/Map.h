#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <string>
#include "SDLInterface.h"
#include "Player.h"
#include "Water.h"
#include "Estimulante.h"
#include "Dog.h"
#include "Obj.h"




class Map
{
	int data_matrix[MAPSIZE_X][MAPSIZE_Y];
	SDLInterface * _graphic;
	int MAX_BLOCK_XCAMARA;
	int MAX_BLOCK_YCAMARA;
	int xCamara;
	int yCamara;
	std::vector<Dog> enemy;
	std::vector<Water> item;
	Player _player;
	Dog _dog;
	Estimulante _estimulante;
	bool end;
	bool win;


public:
	bool start;
	Map(int windowX, int windowY, SDLInterface * graphic);
	~Map();
	//void init(SDLInterface * graphic);
	void drawMap();
	void drawUI();
	void drawPlayer();
	void movePlayer(int direction);
	void doPhysics();
	//Getters and Setters

	void setCamaraX(int camaraX);
	void setCamaraY(int camaraY);
	void setEnd(bool dep);
	void setWin(bool wp);

	int getCamaraX();
	int getCamaraY();
	int getStepsPlayer();
	bool getEnd();
	bool getWin();
	
	void moveCamera(int x, int y);
	void decreesStep();
	void increesStep();
	void drawScoreAtWin();

	void saveGame();
	void loadGame();

};

