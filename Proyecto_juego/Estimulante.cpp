#include "Estimulante.h"



Estimulante::Estimulante()
{
	textureID = SPRITE_ESTIMULANTE;
	x = 5;
	y = 6;
	width = 64;
	height = 64;

}


Estimulante::~Estimulante()
{
}

int Estimulante::getWidth()
{
	return width;
}

int Estimulante::getHeight()
{
	return height;
}

int Estimulante::getPositionX()
{
	return x;
}

int Estimulante::getPositionY()
{
	return y;
}

int Estimulante::getActive()
{
	return active;
}

void Estimulante::updateActive() {
	if(active>0) active--;
	else active = 0;
}

std::string Estimulante::getDirectionX() {
	return directionX;
}

std::string Estimulante::getDirectionY() {
	return directionY;
}

void Estimulante::setPosition(int xValue, int yValue)
{
	x = xValue;
	y = yValue;
}

void Estimulante::setActive(int aktive) {
	active = aktive;
}

void Estimulante::newPosition(Player player)
{
	//he actualizado el spawn de la botella para que se base en el personaje, y asi pueda spawnear por todo el mapa
	//el current steps es para que se genere seg�n lo sobrado que vaya el player tanto en direcci�n negativa como en positiva
	if (player.getCurrentStep() == 0) {
	}
	else {
		x = player.getPositionX() + rand() % (player.getCurrentStep() / 3) - player.getPositionX() + rand() % (player.getCurrentStep() / 4);
		y = player.getPositionY() + rand() % (player.getCurrentStep() / 3) - player.getPositionY() + rand() % (player.getCurrentStep() / 4);
		if (x >= MAPSIZE_X)x = MAPSIZE_X - 1;
		if (y >= MAPSIZE_Y)y = MAPSIZE_Y - 1;
	}
}

bool Estimulante::doPhysics(int xPlayer, int yPlayer)
{
	return x == xPlayer && y == yPlayer;
}


void Estimulante::calculateDirection(Player player) {
	int hor, ver; //horizontal, vertical
	hor = player.getPositionX() - x;
	ver = player.getPositionY() - y;
	if (hor < 0)directionX = "RIGHT";
	else if (hor > 0)directionX = "LEFT";
	else directionX = "OK";
	if (ver < 0)directionY = "DOWN";
	else if (ver > 0)directionY = "UP";
	else directionY = "OK";
}