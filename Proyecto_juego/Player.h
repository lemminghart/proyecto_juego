#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <string>
#include "SDLInterface.h"
#include "Obj.h"



class Player :public Obj
{
	SDLInterface * _graphic;
	int data_matrix[MAPSIZE_X][MAPSIZE_Y];
	int moving; // usado para mover la camara segun el player
	int currentSteps;
	int numSteps;
	int conditionToWin;
	/*
	int _numOptions = 2;
	int MenuX;
	int MenuY;
	glm::ivec2 _mouseCoordinates;*/

public:
	Player();
	~Player();
	void init(SDLInterface * graphic);

	//Getters and Setters
	void setPosition(int xValue, int yValue);
	void setMoving(int valor); //0 = no moving, 1 = X, 2 = Y, both = 3
	void setCurrentStep(int value);
	void setNumStep(int value);
	void setConditionToWin(int condition);

	int getPositionX();
	int getPositionY();
	int getMoving();
	int getCurrentStep();
	int getNumStep();
	int getConditionToWin();

	//Functions
	void UpdateConditionToWin();
	
};


