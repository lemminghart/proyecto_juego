#include "Water.h"



Water::Water()
{
	x = 2;
	y = 2;
	textureID = SPRITE_WATER;
	width = 64;
	height = 64;
	drink = 10;
	rang = 5;
}


Water::~Water()
{
}

int Water::getWidth()
{
	return width;
}

int Water::getHeight()
{
	return height;
}

int Water::getPositionX()
{
	return x;
}

int Water::getPositionY()
{
	return y;
}

int Water::getDrink()
{
	return drink;
}

void Water::setPosition(int xValue, int yValue)
{
	x = xValue;
	y = yValue;
}

std::string Water::getDirectionX() {
	return directionX;
}

std::string Water::getDirectionY() {
	return directionY;
}

void Water::newPosition(Player player)
{
	//he actualizado el spawn de la botella para que se base en el personaje, y asi pueda spawnear por todo el mapa
	//el current steps es para que se genere seg�n lo sobrado que vaya el player tanto en direcci�n negativa como en positiva
	if (player.getCurrentStep() == 0) {
	}
	else {
		x = player.getPositionX() + rand() % (player.getCurrentStep() / 3) - player.getPositionX() + rand() % (player.getCurrentStep() / 4);
		y = player.getPositionY() + rand() % (player.getCurrentStep() / 3) - player.getPositionY() + rand() % (player.getCurrentStep() / 4);
		if (x >= MAPSIZE_X)x = MAPSIZE_X - 1;
		if (y >= MAPSIZE_Y)y = MAPSIZE_Y - 1;
		else drink = rang;
	}
}

bool Water::doPhysics(int xPlayer, int yPlayer)
{
	return x == xPlayer && y == yPlayer;
}


void Water::calculateDirection(Player player) {
	int hor,ver; //horizontal, vertical
	hor = player.getPositionX() - x;
	ver = player.getPositionY() - y;
	if (hor < 0)directionX = "RIGHT";
	else if (hor > 0)directionX = "LEFT";
	else directionX = "OK";
	if (ver < 0)directionY = "DOWN";
	else if (ver > 0)directionY = "UP";
	else directionY = "OK";
}