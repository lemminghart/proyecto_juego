#pragma once
#include "GameConstants.h"
class Obj
{
public:
	int x;
	int y;
	int textureID;
	Obj();
	~Obj();

	void SetX(int value);
	void SetY(int value);
	void SetTextureID(int value);

	int GetX();
	int GetY();
	int GetTextureID();
};

