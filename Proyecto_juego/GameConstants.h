#pragma once

//Game general information
#define GAME_SPEED 0.1f
#define GAME_TEXTURES 11
#define MAX_MONSTERS 10
#define MONSTER_REFRESH_FREQUENCY 500

//Sprite information
#define SPRITE_SPEED 0.01f
#define SPRITE_DEFAULT_WIDTH 64
#define SPRITE_DEFAULT_HEIGHT 64
#define SPRITE_HERO 0
#define SPRITE_FIRE 1
#define SPRITE_MENU 2
#define SPRITE_WORKING 3
#define SPRITE_GRASS 4
#define SPRITE_GRASS2 5
#define SPRITE_WATER 6
#define SPRITE_ESTIMULANTE 7
#define SPRITE_END 8
#define SPRITE_WIN 9
#define SPRITE_DOG 10
#define RANKING 11

//Color information
#define GAME_BASIC_COLORS 5
#define RED 0
#define GREEN 1
#define BLUE 2
#define BLACK 3
#define WHITE 4

//Map
#define MAPSIZE_X 20
#define MAPSIZE_Y 20


#define CONDITION_TO_WIN 10 //decide cuantos items necesitas para ganar el juego
#define RANKING_SIZE 10


//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{ INIT, PLAY, EXIT, MENU, DEP, WIN };

struct ranking {
	char name[50];
	int totalScore;
};
