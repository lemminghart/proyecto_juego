#include "Dog.h"
#include <stdio.h>



Dog::Dog()
{
	x = 8;
	y = 2;
	textureID = SPRITE_DOG;
	time(&timer);
}


Dog::~Dog()
{
}

void Dog::Update(int xValue, int yValue)
{
	time(&act);
	double delta=difftime(act,timer);
	//printf("%.0f",delta);
	if (delta >= 0.1)
	{
		int distanceX = x - xValue;
		int distanceY = y - yValue;
		if (distanceX > 0)x--;
		else if (distanceX < 0)x++;
		if (distanceY > 0)y--;
		else if (distanceY < 0)y++;
		
		timer = act;
	}
}
