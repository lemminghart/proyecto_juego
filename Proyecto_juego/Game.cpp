#include "Game.h"
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <iostream>
#include <time.h>
#include "GameConstants.h"
#include "SDLInterface.h"
#include "InputManager.h"
#include "Sprite.h"
#include "Menu.h"
#include <time.h>
#include "Player.h"
#include "Map.h"
#include <fstream>
#include "ErrorManagement.h"


/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_mapa(_screenWidth, _screenHeight, &_graphic) {
}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
		//Prepare the game components
	init();
		//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	srand((unsigned int)time(NULL));
		//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);	
	_graphic.setWindowBackgroundColor(255, 255, 255, 255);
		//Load the sprites associated to the different game elements
	_graphic.loadTexture(SPRITE_FIRE, "../sharedResources/images/characters/fireSprite.png");
	_graphic.loadTexture(SPRITE_MENU, "../sharedResources/images/desert_menu.jpg");
	_graphic.loadTexture(SPRITE_WORKING, "../sharedResources/images/sorry.png");
	_graphic.loadTexture(SPRITE_HERO, "../sharedResources/images/characters/hero.png");
	_graphic.loadTexture(SPRITE_GRASS, "../sharedResources/images/terrain/sand2.png");
	_graphic.loadTexture(SPRITE_GRASS2, "../sharedResources/images/terrain/sand1.png");
	_graphic.loadTexture(SPRITE_WATER, "../sharedResources/images/water2.png");
	_graphic.loadTexture(SPRITE_ESTIMULANTE, "../sharedResources/images/estimulante.png");
	_graphic.loadTexture(SPRITE_END, "../sharedResources/images/end.jpg");
	_graphic.loadTexture(SPRITE_WIN, "../sharedResources/images/win.png");
	_graphic.loadTexture(SPRITE_DOG, "../sharedResources/images/dog.png");
	_graphic.loadTexture(RANKING, "../sharedResources/images/ranking_menu.jpg");
		//Set the font style
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
		//Initialize the game elements
	_menu.init(&_graphic);
		//The counter used to manage the score
	_gameState = GameState::MENU;
		//iniciamos los textos
	
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	//_gameState = GameState::PLAY;
	
	while (_gameState != GameState::EXIT) {		
			//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
			//Execute the player commands 
		executePlayerCommands();
			//Update the game physics
		doPhysics();
			//Render game
		renderGame();	
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {
	
	//SI SE PULSA EL BOTON IZQUIERDO
	if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
		glm::ivec2 mouseCoords = _graphic.getMouseCoords();
		/*std::cout << mouseCoords.x << " -> X" << std::endl;
		std::cout << mouseCoords.y << " -> Y" << std::endl;*/
		if (_gameState==GameState::MENU) {
			if (mouseCoords.x > 243 && mouseCoords.x < 458) {
				//zona X de botones, valorar si los ha pulsado
				//BOTON 1 = PLAY
				if (mouseCoords.y > 225 && mouseCoords.y < 268) {
					//SE HA PULSADO PLAY
					_gameState = GameState::PLAY;
					startingTime = clock() + startingTime;
					std::cout << "PLAY!" << std::endl;
				}
				//BOTON 2 = RANKING
				if (mouseCoords.y > 311 && mouseCoords.y < 353) {
					verRanking = true;
					std::cout << "RANKING" << std::endl;
				}				
				//BOTON 5 = EXIT
				if (mouseCoords.y > 498 && mouseCoords.y < 541) {
					//SE HA PULSADO EXIT
					std::cout << "EXIT!" << std::endl;
					_gameState = GameState::EXIT;
				}
			}
			if (mouseCoords.y > 405 && mouseCoords.y < 448) {
				//BOTON 3 = SAVE GAME
				if (mouseCoords.x > 104 && mouseCoords.x < 319) {
					_mapa.saveGame();
					std::cout << "PARTIDA GUARDADA!" << std::endl;
				}
				//BOTON 4 = LOAD GAME
				if (mouseCoords.x > 376 && mouseCoords.x < 590) {
					_mapa.loadGame();
					std::cout << "PARTIDA CARGADA!" << std::endl;
				}
			}
		}
		
		
	}

	//MOVIMIENTO HEROE
	// left = 1, right = 2, up = 3, down = 4
	if (_gameState == GameState::PLAY) {
		_mapa.start = true;
		if (_graphic.isKeyPressed(SDLK_LEFT)) {
			//Puedo moverme?
			//Si puedo, actualizo posicion player
					_mapa.movePlayer(2);
					//std::cout << " LEFT" << std::endl;
					_mapa.increesStep();
	
		}
		if (_graphic.isKeyPressed(SDLK_RIGHT)) {
			
					_mapa.movePlayer(4);
					//std::cout << " RIGHT" << std::endl;
					_mapa.increesStep();
				
			
		}
		if (_graphic.isKeyPressed(SDLK_UP)) {
					_mapa.movePlayer(1);
					//std::cout << " UP" << std::endl;
					_mapa.increesStep();
				
			
		}
		if (_graphic.isKeyPressed(SDLK_DOWN)) {
	
					_mapa.movePlayer(3);
					//std::cout << " DOWN" << std::endl;
					_mapa.increesStep();
				
		}
		// Si apreta Esc se cierra
		if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
			_gameState = GameState::MENU;
			
		}
	}

	
	if (_gameState != GameState::PLAY)
	{
		if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
			_gameState = GameState::MENU;
			verRanking = false;
		}
	}
}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	
	if (_gameState == GameState::DEP) {
		_mapa.setEnd(false);
	}
	else if (_gameState == GameState::WIN) {
		
		_mapa.setWin(false);
	}
	else {
		//Como siempre da la booleana true, siempre se pone en win y nunca se puede canviar a menu
		//Se tiene que encontrar la forma de que una vez sea true ya no lo comprueve mas
		_mapa.doPhysics();
		if (_mapa.getEnd() == true)  {
			_gameState = GameState::DEP;
			saveRanking();
			
		}
		if (_mapa.getWin() == true) {
			_gameState = GameState::WIN;
			saveRanking();
			
		}
	}
}

/**
* Render the game on the screen
*/
void Game::renderGame() {
		//Clear the screen
	_graphic.clearWindow();

		//Draw the screen's content based on the game state
	if (_gameState == GameState::MENU) {
		_menu.drawMenu();
		if (verRanking == true) drawRanking();
	}
	else if (_gameState == GameState::DEP) {
		_menu.drawEnd();
	}
	else if (_gameState == GameState::WIN) {
		_menu.drawWin();
		_mapa.drawScoreAtWin();
	}
	else {
		drawGame();

		//_mapa.drawPlayer(40,20);
	}
		//Refresh screen
	_graphic.refreshWindow();
}

/*
* Draw the game menu
*/


/*
* Draw the game, winner or loser screen
*/
void Game::drawGame()	{
	//_graphic.drawTexture(SPRITE_FIRE, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, 1 * SPRITE_DEFAULT_WIDTH, 1* SPRITE_DEFAULT_HEIGHT, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
	//JUNTAR LAS FUNCIONES DENTRO DE MAP. MAP CONTROLA TODOS LOS PINTADOS 
	_mapa.drawMap();
}


/*
* Draw the sprite on the screen
* @param sprite is the sprite to be displayed
*/
void Game::drawSprite(Sprite & sprite) {
	_graphic.drawTexture(sprite.getSpriteId(), SPRITE_DEFAULT_WIDTH*sprite.getCurrentFrame(), 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT
		, sprite.getXAtWorld(), sprite.getYAtWorld(), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
}


void Game::saveRanking()
{
	ranking newPlayer;
	cout << "Please, enter your name: " << endl;
	cin >> newPlayer.name;
	newPlayer.totalScore = _mapa.getStepsPlayer();
	vector <ranking> toCheck;
	loadRanking(toCheck);

	int newPosition;
	bool isInside = false;
	for (int i = 0; i < toCheck.size(); i++) {
		if (_mapa.getStepsPlayer() > toCheck[i].totalScore) {
			newPosition = i;
			isInside = true;
			//is inside the ranking - have more points than someone
			//allocate the correct position
			if (toCheck.size() < RANKING_SIZE) {
				ranking newData;
				toCheck.push_back(newData);
			}
			for (int j = toCheck.size() - 1; j > i; j--) {
				toCheck[j] = toCheck[j - 1];
			}
			toCheck[i] = newPlayer;
			i = toCheck.size();
		}
	}

	//Include the name
	if (!isInside) {
		toCheck.push_back(newPlayer);
	}
	fstream rankingFile;
	rankingFile.open("../sharedResources/DATA/Ranking.bin", std::ios::out | std::ios::trunc | std::ios::binary);
	if (!rankingFile.is_open()) {
		ErrorManagement::errorRunTime("[RANKING-LIST save()] Unable to load the binary file");
	}


	for (int i = 0; i < toCheck.size(); i++) {

		rankingFile.write((char*)&toCheck[i], sizeof(ranking));
	}
	rankingFile.close();
}

void Game::loadRanking(vector <ranking> &ranked)
{
	streampos fileSize;
	fstream rankingFile("../sharedResources/DATA/Ranking.bin", std::ios::in | std::ios::binary);
	if (!rankingFile.is_open()) {
		ErrorManagement::errorRunTime("[RANKING-LIST Load()] Unable to load the binary file");
	}

	rankingFile.seekg(0, std::ios::end);
	fileSize = rankingFile.tellg();
	rankingFile.seekg(0, std::ios::beg);

	//Read the content
	int numElements = fileSize / sizeof(ranking);
	ranking data;
	for (int i = 0; i < numElements; i++) {
		rankingFile.read((char*)&data, sizeof(ranking));
		ranked.push_back(data);
	}
	rankingFile.close();
}

void Game::drawRanking()
{
	_graphic.drawTexture(RANKING, 0, 0, _screenWidth, _screenHeight, 0, 0, _screenWidth, _screenHeight);
	_graphic.drawText("RANKING TOP 10 ", WHITE, BLACK, _screenWidth / 2 - 100, _screenHeight / 4 - 100);
	streampos fileSize;
	fstream rankingFile("../sharedResources/DATA/Ranking.bin", std::ios::in | std::ios::binary);
	if (!rankingFile.is_open()) {
		ErrorManagement::errorRunTime("[RANKING-LIST Load()] Unable to load the binary file");
	}

	rankingFile.seekg(0, std::ios::end);
	fileSize = rankingFile.tellg();
	rankingFile.seekg(0, std::ios::beg);
	//Read the content
	ranking data;
	int numElements = fileSize / sizeof(ranking);
	for (int i = 0; i < numElements; i++) {
		rankingFile.read((char*)&data, sizeof(ranking));
		string show(data.name);
		show += " ";
		show += to_string(data.totalScore);
		_graphic.drawText(show, WHITE, BLACK, _screenWidth / 2 - 100, _screenHeight / 4 - 40 + (i * 35));
	}
	rankingFile.close();

}
