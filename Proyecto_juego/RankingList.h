#pragma once

template <class Data>
class RankingList {
private:
	std::vector <Data *> _elements;
	//The number of elements
	int _iterator;
	int _totalSize;
	int _actualSize;
public:
	RankingList() {
		_actualSize = 0;
		_totalSize = 5;
	};
	~RankingList() {
	};

	//Add element
	void add(Data * p) {
		if (_actualSize == 0) {
			_elements.push_back(p);
			_actualSize++;
		}
		else {
			_elements.push_back(p);
			_actualSize++;

			//Add & Re-order.



			if (_actualSize > _totalSize) {
				_elements.erase(_elements.begin() + _totalSize);
			}
		}
	}

	void saveRanking() {
		begin();
		ofstream rankingFile("../sharedResources/DATA/Ranking.bin", std::ios::binary);
		if (!rankingFile.is_open()) {
			ErrorManagement::errorRunTime("[RANKING-LIST save()] Unable to load the binary file");
		}
		while (!end()) {
			Data * p = nextElement();
			int size = sizeof(p);
			rankingFile.write((Data *)(&p), size);
		}
		rankingFile.close();
		_elements.erase();
	}

	void loadRanking() {
		ifstream rankingFile("../sharedResources/DATA/Ranking.bin", std::ios::binary);
		if (!rankingFile.is_open()) {
			ErrorManagement::errorRunTime("[RANKING-LIST Load()] Unable to load the binary file");
		}
		


		rankingFile.close();
	}
	Data * getElement(int position) {
		return _elements[position];
	}
	void begin() {
		_iterator = 0;
	}

	Data * nextElement() {
		_iterator++;
		return _elements[_iterator - 1];
	}

	bool end() {
		if (_iterator == _elements.size()) return true;
		return false;
	}
};

